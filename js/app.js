const marvel = {
    render: () => {

        const urlAPI = 'https://gateway.marvel.com:443/v1/public/series?ts=1&apikey=c1fc57d90e6f0eacefd07b2fa49fbb4d&hash=3ebe6ea5e23d456d482ef7635de6ecb1';
        const container = document.querySelector('#marvel-row');
        let contentHTML = '';

        fetch(urlAPI)
            .then(res => res.json())
            .then((json) => {
                for (const hero of json.data.results) {
                    let urlHero = hero.urls[0].url;
                    contentHTML += `
                         <div class="col-md-4">
                             <a href="${urlHero}" target="_blank">
                             <img src="${hero.thumbnail.path}.${hero.thumbnail.extension}" alt="${hero.name}" class="img-thumbnail">
                            </a>
                            <h3 class="title">${hero.title}</h3>
                        </div>`;

                }
                container.innerHTML = contentHTML;
            })
    }
};
marvel.render();